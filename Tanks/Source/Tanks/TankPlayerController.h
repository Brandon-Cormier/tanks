// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"


class ATank;
class UTankAimingComponent;
/**
 * 
 */
UCLASS()
class TANKS_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnPossessedTankDeath();

protected: 

	UFUNCTION(BlueprintCallable, Category = "Setup")
	ATank* GetControlledTank() const;

	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void FoundAimingComponent(UTankAimingComponent* AimCompRef);

private:

	UTankAimingComponent* AimingComponent = nullptr;

	void AimTowardsCrosshair();

	bool GetSightRayHitLocation(FVector& HitLocation);

	UPROPERTY( EditDefaultsOnly)
	float CrosshairXPosition = 0.5f;
	UPROPERTY( EditDefaultsOnly)
	float CrosshairYPosition = 0.3f;

	UPROPERTY( EditDefaultsOnly)
	float LineTraceRange = 10000000;

	bool GetLookVectorHitLocation(FVector& HitLocation, FVector WorldLocation, FVector WorldDirection);

	virtual void SetPawn(APawn* InPawn) override;
	
};
