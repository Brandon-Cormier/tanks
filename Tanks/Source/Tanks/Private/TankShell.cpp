// Fill out your copyright notice in the Description page of Project Settings.

#include "TankShell.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"



// Sets default values
ATankShell::ATankShell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("CollisionMesh"));

	SetRootComponent(CollisionMesh);

	CollisionMesh -> SetNotifyRigidBodyCollision(true);

	

	CollisionMesh ->SetVisibility(false);

	LaunchBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("LaunchBlast"));

	LaunchBlast -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ImpactBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("ImpactBlast"));

	ImpactBlast -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ImpactBlast -> bAutoActivate = false;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(FName("MovementComponent"));

	MovementComponent -> bAutoActivate = false;

	ExplosionForce = CreateDefaultSubobject<URadialForceComponent>(FName("ExplosionForce"));

	ExplosionForce -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	

	
}

// Called when the game starts or when spawned
void ATankShell::BeginPlay()
{
	Super::BeginPlay();
	CollisionMesh -> OnComponentHit.AddDynamic(this, &ATankShell::OnHit);
}

// Called every frame
void ATankShell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATankShell::Launch(float Speed)
{
	UE_LOG(LogTemp, Warning, TEXT("FIRE!!!!!"));
	MovementComponent -> SetVelocityInLocalSpace(FVector::ForwardVector * Speed);
	MovementComponent -> Activate();
}


void ATankShell::OnHit(UPrimitiveComponent* HitComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		FVector NormalImpulse,
		const FHitResult& Hit)
{
        LaunchBlast -> Deactivate();
		ImpactBlast -> Activate();
		ExplosionForce ->FireImpulse();

		SetRootComponent(ImpactBlast);
		CollisionMesh -> DestroyComponent();

		UGameplayStatics::ApplyRadialDamageWithFalloff(
			this, 
			ProjectileDamage,
			5.f,
			GetActorLocation(),
			150.f,
			ExplosionForce -> Radius,
			1.2f,
			UDamageType::StaticClass(),
			TArray<AActor*>()
		);



		FTimerHandle TimerHandle;

		GetWorld() -> GetTimerManager().SetTimer(TimerHandle, this, &ATankShell::OnTimerExpire, DestroyTimer, false);
}

void ATankShell::OnTimerExpire()
{
	this -> Destroy();
}

