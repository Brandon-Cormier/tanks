// Made By Brandon Cormier for the GameDev.TV Udemy Unreal C++ Course


#include "SprungWheel.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Components/SphereComponent.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
ASprungWheel::ASprungWheel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Spring = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Spring"));

	SetRootComponent(Spring);


	Axle = CreateDefaultSubobject<USphereComponent>(FName("Axle"));

	Axle -> SetupAttachment(Spring);

	Wheel = CreateDefaultSubobject<USphereComponent>(FName("Wheel"));

	Wheel -> SetupAttachment(Axle);

	AxleConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("AxleConstraint"));

	AxleConstraint -> SetupAttachment(Axle);

	
}

// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();

	SetupConstraints();

	
	
}

// Called every frame
void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//AddDrivingForce(50000000000.f);

}

void ASprungWheel::SetupConstraints()
{
	if(!GetAttachParentActor()){return;}
	UPrimitiveComponent* BodyMass = Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent());
	if (!BodyMass){return;}
	Spring -> SetConstrainedComponents(
		BodyMass,
		NAME_None,
		Axle,
		NAME_None);

	AxleConstraint -> SetConstrainedComponents(
		Axle,
		NAME_None,
		Wheel,
		NAME_None);
}

void ASprungWheel::AddDrivingForce(float Force)
{
	Wheel -> AddTorqueInRadians(Axle -> GetRightVector() * Force, NAME_None, true);
}
