// Fill out your copyright notice in the Description page of Project Settings.

#include "TankBarrel.h"
#include "Math/UnrealMathUtility.h"



void UTankBarrel::Elevate(float RelativeSpeed)
{
    //UE_LOG(LogTemp, Warning, TEXT("elevating at  %f"),DegreesPerSecond);
    RelativeSpeed = FMath::Clamp(RelativeSpeed, -1.f, 1.f);

    auto ElevationChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
    auto RawNewElevation = RelativeRotation.Pitch + ElevationChange;

    auto NewElevation = FMath::Clamp(RawNewElevation, MinElevation, MaxElevation);

    SetRelativeRotation(FRotator(NewElevation, 0, 0));
}
