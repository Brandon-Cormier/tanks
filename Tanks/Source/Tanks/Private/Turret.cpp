// Fill out your copyright notice in the Description page of Project Settings.

#include "Turret.h"
#include "Math/UnrealMathUtility.h"


void UTurret::Rotate(float RelativeSpeed)
{
    //UE_LOG(LogTemp, Warning, TEXT("elevating at  %f"),DegreesPerSecond);
    
    RelativeSpeed = FMath::Clamp(RelativeSpeed, -1.f, 1.f);

    auto RotationChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
    auto RawNewRotation = RelativeRotation.Yaw + RotationChange;

    SetRelativeRotation(FRotator(0, RawNewRotation, 0));
    
}

