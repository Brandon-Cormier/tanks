// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "Tank.h"
#include "DrawDebugHelpers.h"
#include "TankAimingComponent.h"
#include "Engine/World.h"



void ATankPlayerController::BeginPlay()
{
    Super::BeginPlay();
    AimingComponent = GetControlledTank()->FindComponentByClass<UTankAimingComponent>();
    if(!ensure(AimingComponent))
    {
        UE_LOG(LogTemp, Error, TEXT("Player Contoller cannot find aiming component"));
        return;
    }
    FoundAimingComponent(AimingComponent);
    
}

void ATankPlayerController::Tick(float DeltaTime){
    Super::Tick(DeltaTime);
    AimTowardsCrosshair();
}


ATank* ATankPlayerController::GetControlledTank() const
{
    return Cast<ATank>(GetPawn());
}

void ATankPlayerController::AimTowardsCrosshair()
{
    if(!GetControlledTank()){return;}
    if(!ensure(AimingComponent)){return;}
    
    FVector HitLocation; //OutParameter

    if (GetSightRayHitLocation(HitLocation))
    {   
        
        AimingComponent->AimAt(HitLocation);
    }
    

}

bool ATankPlayerController::GetSightRayHitLocation(FVector& HitLocation)
{
    //FindCrosshair position
    int32 ViewportSizeX, ViewportSizeY;
    GetViewportSize(ViewportSizeX, ViewportSizeY);
    auto ScreenLocation = FVector2D(ViewportSizeX * CrosshairXPosition, ViewportSizeY * CrosshairYPosition);


    //De-project the screen position of the crosshair to world direction
    FVector WorldLocation, WorldDirection;
    if(DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, WorldLocation, WorldDirection))
    {
        //Line-Trace along that look direction, see what we hit (up to max range)
        return GetLookVectorHitLocation(HitLocation, WorldLocation, WorldDirection);
            
        
        
    }
    
    
    return false;
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector& HitLocation, FVector WorldLocation, FVector WorldDirection)
{
    FHitResult OutHit;
    auto StartLocation = WorldLocation;
    auto EndLocation = StartLocation + (WorldDirection * LineTraceRange);
    if(GetWorld() ->LineTraceSingleByChannel(
            OutHit,
            StartLocation, 
            EndLocation, 
            ECollisionChannel::ECC_Camera)){
        HitLocation = OutHit.Location;

        /* Debug line for aiming
        DrawDebugLine(
            GetWorld(),
            StartLocation,
            EndLocation,
            FColor(255,0,0),
            false,
            0,
            0,
            1
        );
        */
       
        return true;
    }
    HitLocation = FVector(0.f,0.f,0.f);
    return false;
}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
    Super::SetPawn(InPawn);
    if (InPawn)
    {
        auto PossessedTank = Cast<ATank>(InPawn);
        if (!ensure(PossessedTank)) {return;}
        //subscribe our local method to the tank's death event TODO.
        PossessedTank ->OnTankDeath.AddUniqueDynamic(this, &ATankPlayerController::OnPossessedTankDeath);

    }
    
}

void ATankPlayerController::OnPossessedTankDeath()
{
    StartSpectatingOnly();
    UE_LOG(LogTemp, Warning, TEXT("I HAVE DIED!!!"));
}

