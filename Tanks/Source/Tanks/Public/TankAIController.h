// Fill out your copyright notice in the Description page of Project Settings.

#pragma once



#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

/**
 * 
 */
class ATank;
class UTankAimingComponent;

UCLASS()
class TANKS_API ATankAIController : public AAIController
{
	GENERATED_BODY()

	APawn* PlayerTank = nullptr;

public:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnPossessedTankDeath();
	
private:

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float AcceptanceRadius = 10000.f;

	UTankAimingComponent* AimingComponent = nullptr;

	virtual void SetPawn(APawn* InPawn) override;
};
