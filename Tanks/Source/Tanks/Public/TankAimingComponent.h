// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"


//enum for aiming state
UENUM()
enum class EFiringState : uint8
{
	Reloading,
	Aiming,
	Locked,
	OutOfAmmo
};


class UTankBarrel;
class UTurret;
class ATankShell;




UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKS_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "Setup")
	EFiringState FiringState = EFiringState::Reloading;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Setup")
	int32 ShellCount = 30;

public:	

	void AimAt(FVector HitLocation);

	EFiringState GetFiringState();

	void SetBarrelReference(UTankBarrel* BarrelToSet);

	void SetTurretReference(UTurret* TurretToSet);

	//TODO add Set Turret Reference

	void MoveBarrel();

	void MoveTurret();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialize(UTankBarrel* BarrelToSet, UTurret* TurretToSet);

	UFUNCTION(BlueprintCallable, Category = "Input")
	void Fire();

	virtual void TickComponent(float DeltaTime,enum ELevelTick TickType,FActorComponentTickFunction * ThisTickFunction) override;



private:

	UTankBarrel* Barrel = nullptr;

	UTurret* Turret = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = Firing)
	float LaunchSpeed = 80000; // sensible starting value

	UPROPERTY(EditDefaultsOnly, Category = Firing)
	float ReloadTimeInSeconds = 3.f;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	TSubclassOf<ATankShell> ShellBP; 

	double LastFireTime = 0.f;

	FVector AimDirection = FVector(0.f,0.f,0.f);

	bool IsBarrelMoving();
		
};
